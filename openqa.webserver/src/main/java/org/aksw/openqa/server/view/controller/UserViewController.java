package org.aksw.openqa.server.view.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.aksw.openqa.server.util.MD5Util;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@ManagedBean(name="userViewController")
@SessionScoped
public class UserViewController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7988952280420748804L;

	private static Logger logger = Logger.getLogger(UserViewController.class);
	
	@ManagedProperty(value = "#{applicationViewController}")
	private ApplicationViewController appViewConsole;
	
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;

	public UserViewController() {	
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	public ApplicationViewController getAppViewConsole() {
		return appViewConsole;
	}
	
	public void setAppViewConsole(ApplicationViewController app) {
		appViewConsole = app;
	}
	
	public void change() {
		try {
			String encodeOldPass = MD5Util.encode(oldPassword);
			if(confirmPassword.equals(newPassword) && 
					encodeOldPass.equals(appViewConsole.getPass())) {
				String encodedPass;
				logger.debug("Changing password.");
				try {
					encodedPass = MD5Util.encode(newPassword);
					appViewConsole.setPass(encodedPass);
					
					FacesContext.getCurrentInstance().addMessage(
		                    null,
		                    new FacesMessage(FacesMessage.SEVERITY_INFO,
		                    "Success!",
		                    "Password Changed!"));
					
					logger.debug("Password changed.");
				} catch (Exception e) {
					logger.error("Error changing password.", e);
				}
			} else {
				FacesContext.getCurrentInstance().addMessage(
	                    null,
	                    new FacesMessage(FacesMessage.SEVERITY_WARN,
	                    "Invalid Parameters!",
	                    "Please Try Again!"));
			}
		} catch (Exception e) {
			logger.error("Error changing password.", e);
			FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error!",
                    "An error occurs while trying to change the password, contact the system administrator!"));
		}
	}
}
