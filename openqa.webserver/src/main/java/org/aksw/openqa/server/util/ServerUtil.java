package org.aksw.openqa.server.util;

import java.io.File;

import javax.servlet.ServletContext;

import org.aksw.openqa.manager.plugin.PluginManager;

public class ServerUtil {

	public static PluginManager newPluginManager(ServletContext context) {		
		String contextPath = context.getRealPath(File.separator);
		ClassLoader contextClassLoader = context.getClassLoader();
    	PluginManager pluginManager = new PluginManager(contextPath + "/plugins", contextClassLoader, "openQA-Webserver");
    	return pluginManager;
	}
}
