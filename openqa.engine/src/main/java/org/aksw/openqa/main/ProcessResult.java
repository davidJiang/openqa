package org.aksw.openqa.main;

import java.util.List;

import org.aksw.openqa.component.IComponent;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ResultMetaInfo;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class ProcessResult extends ResultMetaInfo {
		private IResultMapList<? extends IResultMap> output = null;
		private List<? extends IParamMap> input = null;
		private long runtime=0;
		private Throwable exception=null;
		
		public ProcessResult() {
		}
		
		public ProcessResult(ProcessResult processResult) {
			this.output = processResult.getOutput();
			this.input = processResult.getInput();
			this.runtime = processResult.getRuntime();
			this.exception = processResult.getException();
		}

		public IResultMapList<? extends IResultMap> getOutput() {
			return output;
		}

		public void setOutput(IResultMapList<? extends IResultMap> processResults) {
			this.output = processResults;
		}

		public long getRuntime() {
			return runtime;
		}

		public void setRuntime(long runtime) {
			this.runtime = runtime;
		}
		
		public void setComponentSource(IComponent source) {
			this.source = source;
		}

		public Throwable getException() {
			return exception;
		}

		public void setException(Throwable exception) {
			this.exception = exception;
		}

		public List<? extends IParamMap> getInput() {
			return input;
		}

		public void setInput(List<? extends IParamMap> params) {
			this.input = params;
		}
	}