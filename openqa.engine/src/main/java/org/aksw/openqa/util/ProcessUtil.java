package org.aksw.openqa.util;

import java.util.List;

import org.aksw.openqa.component.IProcess;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.providers.impl.ServiceProvider;

public class ProcessUtil {
	
	public static IResultMapList<? extends IResultMap> executedProcess(List<? extends IParamMap> params, 
			IProcess process, ServiceProvider services, 
			IContext context) throws Exception {
		IResultMapList<? extends IResultMap> results = process.process(params, services, context);
		return results;
	}

}
