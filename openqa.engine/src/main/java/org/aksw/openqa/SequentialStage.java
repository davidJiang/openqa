package org.aksw.openqa;

import java.util.List;

import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IProcess;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ResultMapList;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.apache.log4j.Logger;

public class SequentialStage extends Stage {
	
	private static Logger logger = Logger.getLogger(SequentialStage.class);	

	public IResultMapList<? extends IResultMap> process(
			List<? extends IParamMap> params, 
			ServiceProvider services, 
			IContext context, 
			PluginManager pluginManager) {
		IResultMapList<? extends IResultMap> results = null;
		if(!componentIDs.isEmpty()) {
			results = null;
			List<? extends IParamMap> inputParams = params;
			for(String componentID : componentIDs) {
				results = new ResultMapList<IResultMap>();
				IPlugin p = pluginManager.getPlugin(componentID);
				if(p != null) {
					RunProcessVisitor runVisitor = new RunProcessVisitor(inputParams, services, context);
					p.accept(runVisitor);
					results = runVisitor.getResult();
				} else {
					IProcess process = pluginManager.getProcessProvider(componentID);
					if(process != null) {
						try {
							results = process.process(params, services, context);
						} catch (Exception e) {
							logger.error("Error executing process " + process.getId() + " in a sequential stage ", e);
						}
					} else {
						logger.warn("The component " + componentID + " in the dynamic pipeline is not active or does not exists!");
					}
				}
				inputParams = results;
			}
		} else {
			List<? extends IParamMap> inputParams = params;
			for(Stage stage : stages) {
				results = stage.process(inputParams, services, context, pluginManager);			
				inputParams = results;
			}
		}
		return results;
	}

}
