package org.aksw.openqa;

import org.aksw.openqa.component.param.AbstractMap;
import org.aksw.openqa.manager.plugin.PluginManager;

public class SystemVariables extends AbstractMap {
	
	public final static String PLUGIN_MANAGER = "PLUGIN_MANAGER";
	
	private static SystemVariables systemVariables;
	
	public static SystemVariables getInstance() {
		if(systemVariables == null)
			systemVariables = new SystemVariables();
		return systemVariables;
	}
	
	public static PluginManager getPluginManager() {
		return ((PluginManager)(SystemVariables.getInstance().getParam(SystemVariables.PLUGIN_MANAGER)));
	}
}
