package org.aksw.openqa.qald;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.Properties;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.main.ProcessResult;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.qald.schema.Answer;
import org.aksw.openqa.qald.schema.Answers;
import org.aksw.openqa.qald.schema.Dataset;
import org.aksw.openqa.qald.schema.Keywords;
import org.aksw.openqa.qald.schema.Question;
import org.aksw.openqa.util.JAXBUtil;
import org.apache.log4j.Logger;

public class QALDBenchmark {
	
	private static Logger logger = Logger.getLogger(QALDBenchmark.class);
	
	private static final String OUT_OF_SCOPE_MESSAGE = " OUT OF SCOPE ";
	
	public static void serialize(Dataset dataset, File outputFile) throws Exception {
		FileOutputStream fout = new FileOutputStream(outputFile);
		JAXBUtil.serialize(Dataset.class, dataset, fout, "string keywords query");
	}
	
	public static Dataset deserialize(File qaldFile) throws Exception {
		Dataset dataset = JAXBUtil.deserialize(Dataset.class, qaldFile);
		return dataset;
	}
	
	public static Dataset deserialize(InputStream qaldInputStream) throws Exception {
	       JAXBContext jc = JAXBContext.newInstance("org.aksw.openqa.qald.schema");
	       Unmarshaller u = jc.createUnmarshaller();
	       Source source = new StreamSource(qaldInputStream);
	       JAXBElement<Dataset> o = u.unmarshal(source, Dataset.class);
	       return o.getValue();
	}
	
	public void evaluate(File qaldFile, 
			File outputFile, 
			String lang, 
			String queryType, 
			PluginManager pluginManager) throws Exception {
		Dataset dataset = evaluate(qaldFile, lang, queryType, pluginManager);
		serialize(dataset, outputFile);
	}
	
	public static Dataset evaluate(File qaldTest, 
			String lang, 
			String queryType, 
			PluginManager pluginManager) throws Exception {
		Dataset dataset = JAXBUtil.deserialize(Dataset.class, qaldTest);		
		return evaluate(dataset, lang, queryType, pluginManager);
	}
	
	public static Dataset evaluate(Dataset qaldTestDataset, 
			String lang, 
			String queryType, 
			PluginManager pluginManager) {

		logger.debug("Running tests for dataset " + qaldTestDataset.getId());
		
		List<Question> questions = qaldTestDataset.getQuestion();
		Dataset evalDataset = new Dataset();
		evalDataset.setId(qaldTestDataset.getId());
		
		for(Question question : questions) {
			Question evalQuestion = evaluate(question, lang, queryType, pluginManager);
			evalDataset.getQuestion().add(evalQuestion);
		}
		
		return evalDataset;
	}
	
	public static Question evaluate(Question question, 
			String lang, 
			String queryType, 
			PluginManager pluginManager) {
		logger.debug("Running test for Question " + question.getId());
		Question evalQuestion = null;
		if(queryType.contains("keywords")) {
			List<Keywords> stringElements = question.getKeywords();
			for(Keywords query : stringElements) {
				if((lang == null || query.getLang() == null ) || query.getLang().equals(lang)) {
					String queryValue = query.getValue();
					evalQuestion = evaluate(queryValue, question, lang, pluginManager);
				}
			}
		} else {
			List<org.aksw.openqa.qald.schema.String> stringElements = question.getString();
			for(org.aksw.openqa.qald.schema.String query : stringElements) {
				if((lang == null || query.getLang() == null ) || query.getLang().equals(lang)) {
					String queryValue = query.getValue();
					evalQuestion = evaluate(queryValue, question, lang, pluginManager);
				}
			}
		}
		
		return evalQuestion;
	}
	
	public static Question evaluate(String queryValue, Question benchmarkQuestion,
			String lang, 
			PluginManager pluginManager) {
		logger.debug("Running test for Question " + benchmarkQuestion.getId());
		Question evalQuestion = new Question();
		evalQuestion.setId(benchmarkQuestion.getId());
		evalQuestion.setAggregation(benchmarkQuestion.getAggregation());
		evalQuestion.setAnswertype(benchmarkQuestion.getAnswertype());
		evalQuestion.setOnlydbo(benchmarkQuestion.getOnlydbo());
		evalQuestion.getKeywords().addAll(benchmarkQuestion.getKeywords());
		evalQuestion.getString().addAll(benchmarkQuestion.getString());		
		
		logger.debug("Running query: " + queryValue);
		try {
			AnswerFormulation queryProcessor = new AnswerFormulation();
			QueryResult result = queryProcessor.process(queryValue,
					pluginManager);
			List<? extends IResultMap> queryParserResults = result.getParam(QueryResult.Attr.QUERYPARSING_RESULT, 
																				ProcessResult.class).getOutput();
			String generatedQueries = queryParserResultsToString(queryParserResults);
			evalQuestion.setQuery(generatedQueries);
			evalQuestion.setRuntime(result.getRuntime());
			List<? extends IResultMap> entryResults =  result.getOutput();
			if(entryResults != null) {
				evalQuestion.setAnswers(getAnswers(entryResults));
			}
			evalQuestion.setException(result.getException());
		} catch (Exception e) {
			evalQuestion.setException(e);
			logger.error("Erro executing query: " + queryValue, e);
		}		
		return evalQuestion;
	}
	
	public static String queryParserResultsToString(List<? extends IResultMap> queryParserResults) {
		String sparqlQueriesAnswer = null;
		if(queryParserResults != null && queryParserResults.size() > 0) {
			for(IResultMap interpreterResult : queryParserResults) {
				String sparqlParam = (String) interpreterResult.getParam(Properties.SPARQL);
				if(sparqlParam != null) {									
					if(sparqlQueriesAnswer != null) {
						sparqlQueriesAnswer += sparqlParam + ";";
					} else {
						sparqlQueriesAnswer = sparqlParam + ";";
					}
				}
			}
		}
		
		if(sparqlQueriesAnswer == null) {
			sparqlQueriesAnswer = QALDBenchmark.OUT_OF_SCOPE_MESSAGE;
		}
		
		return sparqlQueriesAnswer;
	}
	
	public static QALDBenchmarkResult evaluate(File actualAnwersFile, 
			File expectedAnswersFile) throws Exception {
		Dataset yourDataset = deserialize(actualAnwersFile);
		Dataset targetDataset = deserialize(expectedAnswersFile);
		return evaluate(yourDataset, targetDataset);
	}
	
	public static QALDBenchmarkResult evaluate(InputStream actualAnwersStream, 
			InputStream expectedAnswersStream) throws Exception {
		Dataset yourDataset = deserialize(actualAnwersStream);
		Dataset targetDataset = deserialize(expectedAnswersStream);
		return evaluate(yourDataset, targetDataset);
	}
	
	public static QALDBenchmarkResult evaluate(Dataset actual, 
			Dataset expected) throws Exception {
		
		QALDBenchmarkResult qaldBenchmarkResult = new QALDBenchmarkResult();
		
		List<Question> givenAnswers = actual.getQuestion();
		List<Question> targetAnswers = expected.getQuestion();
		
		for(Question targetQ : targetAnswers) {
			Question answeredQuestion = getQuestion(targetQ.getId(), givenAnswers);
			QuestionResult qResult = new QuestionResult();
			if(answeredQuestion != null) {
				qResult = evaluate(answeredQuestion, targetQ);
			}
			qaldBenchmarkResult.add(qResult);
		}
		
		return qaldBenchmarkResult;
	}
	
	public static QuestionResult evaluate(Question actual,
			Question expected) throws Exception {

		QuestionResult qResult = new QuestionResult();
		double precision = EvaluationUtils.precision(expected, actual);
		double fmeasure = EvaluationUtils.fMeasure(expected, actual);
		double recall = EvaluationUtils.recall(expected, actual);

		qResult.setQuestionID(actual.getId());
		qResult.setFmeasure(fmeasure);
		qResult.setRecall(recall);
		qResult.setPrecision(precision);

		return qResult;
	}
	
	public static Question getQuestion(String id, Dataset dataset) {
		return getQuestion(id, dataset.getQuestion());
	}
	
	public static Question getQuestion(String id, List<Question> questions) {
		for(Question question : questions) {
			if(question.getId().equals(id)) {
				return question;
			}
		}
		return null;
	}
	
	private static Answers getAnswers(List<? extends IResultMap> entryResults) {
		Answers answers = new Answers();
		for(IResultMap entryrResult : entryResults) {
			String attrValue = null;
			Answer answer = null;

			if(entryrResult.contains(Properties.URI)) {
				attrValue = (String) entryrResult.getParam(Properties.URI);		
				answer = new Answer();
				answer.setUri(attrValue);
			} else if (entryrResult.contains(Properties.Literal.NUMBER)) {
				attrValue = entryrResult.getParam(Properties.Literal.NUMBER).toString();
				answer = new Answer();
				answer.setNumber(attrValue);
			} else if (entryrResult.contains(Properties.Literal.DATE)) {
				attrValue = (String) entryrResult.getParam(Properties.Literal.DATE);
				answer = new Answer();
				answer.setDate(attrValue);
			} else if (entryrResult.contains(Properties.Literal.BOOLEAN)) {
				attrValue = (String) entryrResult.getParam(Properties.Literal.BOOLEAN);
				answer = new Answer();
				answer.setBoolean(attrValue);
			} else if(entryrResult.contains(Properties.RESOURCE)) {
				attrValue = (String) entryrResult.getParam(Properties.RESOURCE);
				answer = new Answer();
				answer.setUri(attrValue);
			}

			if(answer != null) {
				answers.getAnswer().add(answer);
			}
		}
		return answers;
	}

}
