package org.aksw.openqa.qald;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;

public class QALDBenchmarkResult extends ArrayList<QuestionResult> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7694073500728608223L;

	public double getPrecision() {
		
		if(size() == 0) {
			return 0;
		}
		
		double precision = 0;
		for(QuestionResult qResult: this) {
			precision += qResult.getPrecision();
		}
		return precision / size();
	}
	
	public double getRecall() {
		if(size() == 0) {
			return 0;
		}
		
		double recall = 0;
		for(QuestionResult qResult: this) {
			recall += qResult.getRecall();
		}
		return recall / size();
	}
	
	public double getFmeasure() {
		if(size() == 0) {
			return 0;
		}
		
		double fmeasure = 0;
		for(QuestionResult qResult: this) {
			fmeasure += qResult.getFmeasure();
		}
		return fmeasure / size();
	}
	
	public int getPartialAnsweredQueries() {		
		int partial = 0;
		for(QuestionResult qResult: this) {
			if(qResult.getRecall() > 0 &&
					qResult.getRecall() < 1) {
				partial++;
			}
		}
		return partial;
	}
	
	public int getFullAnsweredQueries() {		
		int full = 0;
		for(QuestionResult qResult: this) {
			if(qResult.getRecall() == 1) {
				full++;
			}
		}
		return full;
	}
	
	@Override
	public String toString() {
		String result = "";
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try(PrintStream ps = new PrintStream(baos)) {
				for(QuestionResult qResult: this) {
					ps.println(qResult.toString());
				}			
				ps.println("Overall Recall:" + getRecall() + 
					" Precision:" + getPrecision() + 
					" F-measure:" + getFmeasure() +
					" Fully Answered Queries:" + getFullAnsweredQueries() +
					" Partial Answered Queries:" + getFullAnsweredQueries());
			}
			result = baos.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
