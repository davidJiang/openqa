package org.aksw.openqa.component.answerformulation.queryparser.impl;

import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aksw.openqa.Properties;
import org.aksw.openqa.component.answerformulation.AbstractQueryParser;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.service.cache.ICacheService;
import org.aksw.openqa.main.OpenQA;
import org.apache.log4j.Logger;

import HMMQuerySegmentation.Constants;

import com.Layout.ConnectionClass;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class SinaQueryParser extends AbstractQueryParser {
	
	private static Logger logger = Logger.getLogger(SinaQueryParser.class);
	
	// Component params
	public final static String END_POINT_PARAM = "END_POINT";
	public final static String DEFAULT_GRAPH_PARAM = "DEFAULT_GRAPH";
	
	// Static variables
	public final static String CACHE_CONTEXT = "SINA";
	
	private Driver driver = new org.sqlite.JDBC();
	
	public SinaQueryParser(Map<String, Object> params) {
		super(params);
	}
	
	@Override
	public boolean canProcess(IParamMap token) {
		String q = (String) token.getParam(Properties.Literal.TEXT);
		return q != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<? extends IResultMap> process(IParamMap paramMap, ServiceProvider serviceProvider, IContext context) throws Exception {
		ICacheService cacheService = serviceProvider.get(ICacheService.class);
		String query = (String) paramMap.getParam(Properties.Literal.TEXT);
		List<String> queryList = cacheService.get(CACHE_CONTEXT, query, List.class);
		
		if(queryList == null) {
			ConnectionClass c=new ConnectionClass();
			c.setSearchQuery(query);
			c.runquery();
			queryList = c.getSPARQLquery_list();
			if(queryList != null && !queryList.isEmpty()) {
				cacheService.put(CACHE_CONTEXT, query, queryList);
			}
		}
		
		List<IResultMap> results = new ArrayList<IResultMap>();
		for(String sparql : queryList) {
			ResultMap r = new ResultMap();
			r.setParam(Properties.SPARQL, sparql);
			results.add(r);
		}
		return results;
	}
	
	@Override
	public void setProperties(Map<String, Object> params) {
		String endPoint = (String) params.get(END_POINT_PARAM);
		Constants.SPARQLEndPoint = endPoint;
		Model.Constants.SPARQLEndPoint = endPoint;
		
		String defaultGraph = (String) params.get(DEFAULT_GRAPH_PARAM);
		Constants.DefaultGraphString = defaultGraph;
		Model.Constants.DefaultGraphString = defaultGraph;
		super.setProperties(params); // saving parameters into the Interpreter
	}
	
	@Override
	public void startup() {
		try {
			java.sql.DriverManager.registerDriver(driver);
		} catch (SQLException e) {
			logger.warn("The driver could not be deregistered.", e);
		}
	}
	
	@Override
	public void shutdown() {
		try {
			java.sql.DriverManager.deregisterDriver(driver);
		} catch (SQLException e) {
			logger.warn("The driver could not be deregistered.", e);
		}
	}
	
	@Override
	public String getVersion() {
		return OpenQA.ENGINE_VERSION;
	}
	
	@Override
	public String getAPI() {
		return OpenQA.API_VERSION;
	}
}
